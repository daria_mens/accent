toggleNav();
countNum();

// Menu
function toggleNav(){
    $(".open-btn").click(function(e){
        $(".block-nav").addClass("open-nav");
        document.body.style.overflowY = "hidden";
        e.stopPropagation();
    });
    $(".mob-btn").click(function(){
        $(".block-nav").removeClass("open-nav");
        document.body.style.overflowY = "auto";
    });

    $("body").click(function(){
        if($('.block-nav').hasClass("open-nav")){
            $(".block-nav").removeClass("open-nav");
            document.body.style.overflowY = "auto";
        }

    });
    $(".block-nav").click(function(e){
        e.stopPropagation();
    });

}
// Sub Menu
window.menuBtnIsActive = false;
menuClick();
function menuClick() {
    var checkMobile = isMobile();

    if (checkMobile == true) {
        if( window.menuBtnIsActive == false){
            $('.menu-arrow-bottom').on('click', function(){
                var sub = $(this).next('.nav-category-sub');
                if(sub.hasClass('expand')) {
                    sub.removeClass('expand')
                } else {
                    sub.addClass('expand')
                }
            })

            window.menuBtnIsActive = true;
        }

    } else {
        $('.menu-arrow-bottom').unbind('click')
        window.menuBtnIsActive = false;
    }
}

$(window).on('resize', function(){
    menuClick();
});

$(window).on('click', function(){
    $('.nav-category-sub').removeClass('expand')
});
$('.nav-category-li, ul').on('click', function(e){
    e.stopPropagation()
});

function isMobile(){
    var isMobile = false;

    if ($(window).width() <= 850) {
        isMobile = true
    }

    return isMobile;
}
// main slider
var mainSlider = new Swiper('#main-slider .swiper-container', {
    nextButton: '#main-slider .slider-arrow-right',
    prevButton: '#main-slider .slider-arrow-left',
    spaceBetween: 20,
    autoplay: 4500,
    speed:1000,
    loop: true
});
// Parteners
var parteners = new Swiper('.parteners .swiper-container', {
    nextButton: '.parteners .swiper-button-next',
    prevButton: '.parteners .swiper-button-prev',
    slidesPerView: 8,
    spaceBetween: 100,
    loop:true,
    breakpoints: {
        1024: {
            slidesPerView: 6,
            spaceBetween: 40,
            autoplay: 4500,
            speed:1000
        },
        768: {
            slidesPerView: 5,
            spaceBetween: 30
        },
        640: {
            slidesPerView: 4,
            spaceBetween: 20
        },
        320: {
            slidesPerView: 2,
            spaceBetween: 10
        }
    }
});
// Parteners-header
var parteners2 = new Swiper('.parteners-header .swiper-container', {
    slidesPerView: 8,
    spaceBetween: 50,
    loop:true,
    breakpoints: {
        1024: {
            slidesPerView: 6,
            spaceBetween: 40,
            autoplay: 4500,
            speed:1000
        },
        768: {
            slidesPerView: 5,
            spaceBetween: 30
        },
        640: {
            slidesPerView: 4,
            spaceBetween: 20
        },
        320: {
            slidesPerView: 2,
            spaceBetween: 10
        }
    }
});


function countNum() {
    var options = {
        useEasing : true,
        useGrouping : true,
        separator : ',',
        decimal : '.',
        prefix : '',
        suffix : ''
    };
    var test =  new CountUp("repare", 0, 5555, 0, 2.5, options);
    $(window).load(function(){
        test.start();
        });

}
$(document).ready(function(){
    $(window).scroll(function(){
        var scr = $("body").scrollTop();
        $('#hid').text(scr);
        if ( scr > 200 ) { $(".fixed-nav").addClass('show-nav'); } else { $(".fixed-nav").removeClass('show-nav'); };
    })

})
$(function(){
    var $gallery = $('.gallery a').simpleLightbox();

    $gallery.on('show.simplelightbox', function(){
        console.log('Requested for showing');
    })
        .on('shown.simplelightbox', function(){
            console.log('Shown');
        })
        .on('close.simplelightbox', function(){
            console.log('Requested for closing');
        })
        .on('closed.simplelightbox', function(){
            console.log('Closed');
        })
        .on('change.simplelightbox', function(){
            console.log('Requested for change');
        })
        .on('next.simplelightbox', function(){
            console.log('Requested for next');
        })
        .on('prev.simplelightbox', function(){
            console.log('Requested for prev');
        })
        .on('nextImageLoaded.simplelightbox', function(){
            console.log('Next image loaded');
        })
        .on('prevImageLoaded.simplelightbox', function(){
            console.log('Prev image loaded');
        })
        .on('changed.simplelightbox', function(){
            console.log('Image changed');
        })
        .on('nextDone.simplelightbox', function(){
            console.log('Image changed to next');
        })
        .on('prevDone.simplelightbox', function(){
            console.log('Image changed to prev');
        })
        .on('error.simplelightbox', function(e){
            console.log('No image found, go to the next/prev');
            console.log(e);
        });
});
// sticky sidebar
$( document ).ready(function() {
    $('.sticky_column').stick_in_parent({
        offset_top: 100

    });
});
$('.sticky_column')
.on('sticky_kit:bottom', function(e) {
    $(this).parent().css('position', 'static');
})
.on('sticky_kit:unbottom', function(e) {
    $(this).parent().css('position', 'relative');
})
// End sticky sidebar
jQuery(function($) {

    $('.block-rules').asScrollable();
});
var modal = (function() {

    // Cache
    var $modal = $('.modal')
    var $btn = $('.openModal')
    var $close = $modal.find('.modal__close')

    // Events
    $btn.on('click', showModal)
    $modal.on('click', hideModal)

    // Functions
    function showModal(e){
        var id = $(e.target).attr('data-modal')
        $(id).addClass('show')
    }

    function hideModal(e){
        if ($(e.target).hasClass('modal__close') ||
            $(e.target).hasClass('modal')) {

            $modal.removeClass('show')
        }
    }

    return {
        modals: $modal
    }

})()

// TABS
$(document).ready(function(){

    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current-tab');
        $('.tab-content').removeClass('current-tab');

        $(this).addClass('current-tab');
        $("#"+tab_id).addClass('current-tab').fadeIn();
    })

});

